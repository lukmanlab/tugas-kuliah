import java.util.Scanner;

public class PlantMain {
    public static void main(String[] args) {

    Garden g = new Garden();
    g.mGardenName = "Taman Surga";

    Scanner sc = new Scanner(System.in);
    int inp = 0;
    do{
        System.out.println();
        System.out.println("0. Display Tanaman");
        System.out.println("1. Tambahkan Tanaman");
        System.out.println("2. Tambah Air");
        System.out.println("3. Tambah Pupuk");
        System.out.println("4. Panen");
        System.out.println("5. Lihat Nilai");
        System.out.println("99. => KELUAR \n");
        System.out.print("Pilih: "); inp = sc.nextInt();
        System.out.print("\033[H\033[2J");
        switch (inp) {
            case 0:
                g.displayGarden();
                break;

            case 1:
                g.addPlant();
                break;

            case 2:
                g.beriAir();
                break;

            case 3:
                g.beriPupuk();
                break;
            
            case 4:
                if (g.harvestPlant() == 1){
                    System.out.println("Berhasil memanen tanaman.");
                }else{
                    System.out.println("Tidak ada tanaman dewasa/berbunga di Taman!");
                }
                break;

            case 5:
                System.out.printf("Nilai Panen: %d\n", g.getNilai());
                break;

            default:
                break;
        }
    }while(inp != 99);

    sc.close();

    }
}