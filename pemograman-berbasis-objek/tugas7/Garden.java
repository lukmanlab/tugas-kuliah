import java.util.ArrayList;

public class Garden {
  String mGardenName;
  ArrayList<Plant> mArrayList = new ArrayList<Plant>();
  int hasilPanen;
  int capacity = 10;

  public void addPlant() {
    if (mArrayList.size() < capacity){
      mArrayList.add(new Plant());
      System.out.println("Tanaman ditambahkan.");
    }else{
      System.out.println("Taman sudah penuh.");
    }
  }

  public void beriAir() {
    for (Plant plant : mArrayList) {
      plant.beriAir();
    }
  }

  public void beriPupuk() {
    for (Plant plant : mArrayList) {
      plant.beriPupuk();
    }
  }

  public int harvestPlant() {
    for (Plant plant : mArrayList) {
      if(plant.statusTumbuh >= 3 ){
        hasilPanen++;
        mArrayList.remove(plant);
        return 1;
      }
    }
    return 0;
  }

  public int getNilai() {
    return hasilPanen * 100;
  }

  public void displayGarden() {
    
    if (mArrayList.size() == 0){
      System.out.println("=============================\n");
      System.out.println("TIDAK ADA TANAMAN DI TAMAN! \n");
      System.out.println("=============================");
    }

    int i = 0;
    for (Plant plant : mArrayList) {
      i++;
      System.out.printf("=========\nTanaman %d\n=========\n", i);
      System.out.printf("Status Tumbuh: %d", plant.statusTumbuh);
      System.out.printf("\tStatus Jumlah Air: %d", plant.jumlahAir);
      System.out.printf("\tStatus Jumlah Pupuk: %d", plant.jumlahPupuk);
      System.out.printf("\tBentuk: %s\n\n", plant.getStatusTumbuhText());
    }
  }

}
