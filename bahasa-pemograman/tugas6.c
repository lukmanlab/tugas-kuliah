#include <stdio.h>
#include <stdbool.h>

void cetakGaris(int x){
  for (int i = 0; i<x; ++i){
    printf("* ");
  }
}

bool isPositive(int x){
  if (x > 0){
    return true;
  }else {
    return false;
  }
}

void faktorBilangan(int a, int b){
  // a = bilangan
  // b = faktor bilangan 
  if (b % a == 0){
    printf("Ya, %d termasuk faktor bilangan %d.", a, b);
  }else{
    printf("Tidak, %d tidak termasuk faktor bilangan %d.", a, b);
  }
}

void menu(int x){
  int n,m,a,b;

  switch (x) {
  case 1:
      cetakGaris(10);
    break;
  case 2:
      printf("Masukkan Angka: ");
      scanf("%d", &m);
      if (isPositive(m) == true){
        printf("Betul, bilangan tersebut Positif.");
      }else{
        printf("Betul, bilangan tersebut Negatif.");
      }
    break;
  case 3:
      printf("Masukkan bilangan: ");
      scanf("%d", &a);
      printf("Masukkan faktor bilangan: ");
      scanf("%d", &b);
      faktorBilangan(a,b);
    break;
  default:
    break;
  }
}

int main(){
  int opsi;
  printf("1. Cetak Garis (*) \n");
  printf("2. Cek bilangan Positif atau Tidak \n");
  printf("3. Faktor Bilangan \n");
  printf("Pilih: "); scanf("%d", &opsi);
  menu(opsi);
}