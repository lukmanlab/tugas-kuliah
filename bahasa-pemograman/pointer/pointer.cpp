#include <iostream>
using namespace std;

int main(){

    /*
        Contoh menampilkan alamat Memory sebuah Variable
    */

    // Contoh melihat alamat memory Variable
    int nilaiSatu = 100;
    cout << "Alamat memory variable nilaiSatu: " << &nilaiSatu;

    /*
        Contoh 1
    */

    // Deklarasi variable Pointer Integer
    int *pointerVariable1;

    // Deklarasi variable Biasa Integer
    int variableBiasaInt;
    variableBiasaInt = 200;

    // Menetapkan alamat variableBiasaInt kepada pointerVariable1
    pointerVariable1 = &variableBiasaInt;

    // Mengakses nilai pointerVariable1
    cout << endl << "Nilai ponterVariable adalah: " << *pointerVariable1;

    /*
        Contoh 2
    */

    // Deklarasi variable Pointer String
    string *pointerVariable2;

    // Deklarasi variable Biasa String
    string variableBiasaStr;
    variableBiasaStr = "lukman hakim";

    // Menetapkan alamat variableBiasaStr kepada pointerVariable2
    pointerVariable2 = &variableBiasaStr;

    // Mengakses nilai pointerVariable2
    cout << endl << "Nilai ponterVariable adalah: " << *pointerVariable2;


    return 0;
}